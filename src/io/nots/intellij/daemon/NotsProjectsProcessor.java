package io.nots.intellij.daemon;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.PsiFile;
import io.nots.intellij.ProjectFilesUtils;
import io.nots.intellij.ui.ApiKeyManagement;

import java.util.ArrayList;
import java.util.List;

public class NotsProjectsProcessor implements Runnable {


    @Override
    public void run() {

        LineMarkerManager lineMarkerManager = LineMarkerManager.getInstance();
        Project[] projects = ProjectManager.getInstance().getOpenProjects();
        List<Project> processedProjects = new ArrayList<>();
        for (Project project : projects) {
            String sha = ProjectFilesUtils.getProjectRootGitSHA(project);
            PropertiesComponent propertiesComponent = PropertiesComponent.getInstance(project);
            String userApiKey = propertiesComponent.getValue(ApiKeyManagement.IO_NOTS_USER_API_KEY);
            String projectKey = propertiesComponent.getValue(ApiKeyManagement.IO_NOTS_PROJECT_KEY);
            if (sha != null && !StringUtil.isEmptyOrSpaces(userApiKey) && !StringUtil.isEmptyOrSpaces(projectKey)) {
                List<PsiFile> files = ProjectFilesUtils.allOpenedFilesUnderProjectRoot(project);
                for (PsiFile psiFile: files) {
                    String fileName = ProjectFilesUtils.basePathForFile(psiFile);
                    ApplicationManager.getApplication().executeOnPooledThread(new NotsFileProcessor(lineMarkerManager, psiFile, sha, userApiKey, projectKey,fileName));
                }
                lineMarkerManager.cleanupFiles(files, project);
                processedProjects.add(project);
            }
        }
        lineMarkerManager.cleanupProject(processedProjects);

    }
}

